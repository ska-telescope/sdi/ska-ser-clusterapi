.DEFAULT_GOAL := help
SHELL=/usr/bin/env bash

.PHONY: vars help check-env playbooks orch
.DEFAULT_GOAL := help

THIS_BASE=$(shell pwd)


DATACENTRE ?= stfc-techops
ENVIRONMENT ?= production
SERVICE ?= clusterapi
PLAYBOOKS_HOSTS ?= clusterapi
GITLAB_PROJECT_ID ?= 39377838
TF_HTTP_USERNAME ?=

BASE_PATH?=$(shell cd "$(dirname "$1")"; pwd -P)
ENVIRONMENT_ROOT_DIR?=$(BASE_PATH)/datacentres/$(DATACENTRE)/$(ENVIRONMENT)
TF_ROOT_DIR?=$(ENVIRONMENT_ROOT_DIR)/$(SERVICE)/orchestration
TF_HTTP_ADDRESS?=https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT_ID)/terraform/state/$(DATACENTRE)-$(ENVIRONMENT)-$(SERVICE)-terraform-state
TF_HTTP_LOCK_ADDRESS?=https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT_ID)/terraform/state/$(DATACENTRE)-$(ENVIRONMENT)-$(SERVICE)-terraform-state/lock
TF_HTTP_UNLOCK_ADDRESS?=https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT_ID)/terraform/state/$(DATACENTRE)-$(ENVIRONMENT)-$(SERVICE)-terraform-state/lock
# TF_HTTP_ADDRESS = "https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT_ID)/terraform/state/$(DATACENTRE)-clusterapi-terraform-state"
# TF_HTTP_LOCK_ADDRESS = "https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT_ID)/terraform/state/$(DATACENTRE)-clusterapi-terraform-state/lock"
# TF_HTTP_UNLOCK_ADDRESS = "https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT_ID)/terraform/state/$(DATACENTRE)-clusterapi-terraform-state/lock"


PLAYBOOKS_ROOT_DIR?=$(ENVIRONMENT_ROOT_DIR)/installation
TF_INVENTORY_DIR?=$(PLAYBOOKS_ROOT_DIR)
INVENTORY?=$(PLAYBOOKS_ROOT_DIR)
ANSIBLE_CONFIG?=$(PLAYBOOKS_ROOT_DIR)/ansible.cfg
ANSIBLE_SSH_ARGS?=-o ControlPersist=30m -o StrictHostKeyChecking=no -F $(PLAYBOOKS_ROOT_DIR)/ssh.config
ANSIBLE_COLLECTIONS_PATHS?=$(BASE_PATH)/ska-ser-ansible-collections


# TF_ROOT_DIR ?= "$(THIS_BASE)/environments/$(ENVIRONMENT)/orchestration"
# PLAYBOOKS_ROOT_DIR ?= "$(THIS_BASE)/environments/$(ENVIRONMENT)/installation"
# ANSIBLE_COLLECTIONS_PATHS ?= "$(THIS_BASE)/ska-ser-ansible-collections"
# ANSIBLE_CONFIG ?= "$(THIS_BASE)/environments/$(ENVIRONMENT)/installation/ansible.cfg"


# clusterctl vars



CAPO_CLUSTER ?= capo-cluster
KUSTOMIZE_OVERLAY ?= base
# https://github.com/kubernetes-sigs/cluster-api/releases
CLUSTERCTL_VERSION ?= v1.2.7
# https://kubernetes.io/releases/
CAPO_K8S_VERSION ?= v1.25.4
CAPO_VERSION ?= v0.6.4
KUBE_NAMESPACE ?= default
CAPO_CLOUDS_PATH ?= clouds.yaml
# https://github.com/mikefarah/yq/releases
YQ_VERSION ?= 4.30.5## yq tool version to install
# https://github.com/helm/helm/releases
HELM_VERSION ?= v3.10.2
CONTROL_PLANE_COUNT ?= 3
WORKER_COUNT ?= 2
# https://github.com/projectcalico/calico/releases
CALICO_VERSION ?= 3.24.5
# https://github.com/kubernetes/ingress-nginx/releases
NGINX_VERSION ?= 1.3.1
METALLB_VERSION ?= 0.13.4
# https://github.com/rook/rook/releases
ROOK_VERSION ?= release-1.10
CIDR_BLOCK ?= 10.10.0.0/16
NETWORK_NAME ?= SKA-TechOps-ClusterAPI1
SUBNET_NAME ?= SKA-TechOps-ClusterAPI-Net1
IP_AUTODETECTION_METHOD ?= interface=ens.*,eth.*
CALICO_IPV4POOL_IPIP ?= Never


.DEFAULT_GOAL := help

.PHONY: capo-cluster help


## OpenStack args
OPENSTACK_FAILURE_DOMAIN ?= ceph
OPENSTACK_IMAGE_NAME ?= ubuntu-2004-kube-$(CAPO_K8S_VERSION)
OPENSTACK_CLOUD ?= skatechops
CLUSTER_API_OPENSTACK_INSTANCE_CREATE_TIMEOUT ?= 15

OPENSTACK_CONTROL_PLANE_MACHINE_FLAVOR ?= l3.micro
OPENSTACK_DNS_NAMESERVERS ?=
OPENSTACK_NODE_MACHINE_FLAVOR ?= l3.tiny
OPENSTACK_SSH_KEY_NAME ?= ska-techops
CAPO_CLOUD ?= skatechops

## Private OpenStack vars
OPENSTACK_EXTERNAL_NETWORK_ID ?=
OPENSTACK_CLOUD_YAML_B64 ?=
OPENSTACK_CLOUD_CACERT_B64 ?=
OPENSTACK_CLOUD_PROVIDER_CONF_B64 ?=

# Rook/Ceph vars
ETC_CEPH ?= /etc/ceph
ETC_CEPH_CONF ?= $(ETC_CEPH)/ceph.conf
ETC_CEPH_CLIENT_ADMIN_KEY_RING ?= $(ETC_CEPH)/ceph.client.admin.keyring
CEPH_NAMESPACE ?= rook-ceph
RBD_POOL_NAME ?= volumes
RGW_POOL_PREFIX ?= default
CSI_RBD_NODE_SECRET_NAME ?= csi-rbd-node
CSI_RBD_PROVISIONER_SECRET_NAME ?= csi-rbd-provisioner
CSI_CEPHFS_NODE_SECRET_NAME ?= csi-cephfs-node
CSI_CEPHFS_PROVISIONER_SECRET_NAME ?= csi-cephfs-provisioner


# define private rules and additional makefiles
-include $(BASE_PATH)/PrivateRules.mak


# Our pipeline machinery
-include .make/base.mk
# -include .make/bats.mk
-include .make/infra.mk
-include .make/terraform.mk
# -include .make/python.mk

TF_EXTRA_VARS ?= DATACENTRE="$(DATACENTRE)" \
	ENVIRONMENT="$(ENVIRONMENT)" \
	SERVICE="$(SERVICE)" \
	TF_HTTP_USERNAME="$(TF_HTTP_USERNAME)" \
	TF_HTTP_PASSWORD="$(TF_HTTP_PASSWORD)" \
	BASE_PATH="$(BASE_PATH)" \
	GITLAB_PROJECT_ID="$(GITLAB_PROJECT_ID)" \
	ENVIRONMENT_ROOT_DIR="$(ENVIRONMENT_ROOT_DIR)" \
	TF_ROOT_DIR="$(TF_ROOT_DIR)" \
	TF_HTTP_ADDRESS="$(TF_HTTP_ADDRESS)" \
	TF_HTTP_LOCK_ADDRESS="$(TF_HTTP_LOCK_ADDRESS)" \
	TF_HTTP_UNLOCK_ADDRESS="$(TF_HTTP_UNLOCK_ADDRESS)" \
	PLAYBOOKS_ROOT_DIR="$(PLAYBOOKS_ROOT_DIR)" \
	TF_INVENTORY_DIR="$(TF_INVENTORY_DIR)" \
	INVENTORY="$(INVENTORY)" \
	ANSIBLE_CONFIG="$(ANSIBLE_CONFIG)" \
	ANSIBLE_SSH_ARGS="$(ANSIBLE_SSH_ARGS)" \
	ANSIBLE_COLLECTIONS_PATHS="$(ANSIBLE_COLLECTIONS_PATHS)" \
	AZUREAD_CLIENT_ID=$(AZUREAD_CLIENT_ID) \
	AZUREAD_CLIENT_SECRET=$(AZUREAD_CLIENT_SECRET) \
	AZUREAD_COOKIE_SECRET=$(AZUREAD_COOKIE_SECRET) \
	AZUREAD_TENANT_ID=$(AZUREAD_TENANT_ID) \
	SLACK_API_URL=$(SLACK_API_URL) \
	SLACK_API_URL_USER=$(SLACK_API_URL_USER) \
	PROM_OS_AUTH_URL=$(PROM_OS_AUTH_URL) \
	PROM_OS_USERNAME=$(PROM_OS_USERNAME) \
	PROM_OS_PASSWORD=$(PROM_OS_PASSWORD) \
	PROM_OS_PROJECT_ID=$(PROM_OS_PROJECT_ID) \
	GITLAB_TOKEN=$(GITLAB_TOKEN) \
	KUBECONFIG=$(KUBECONFIG) \
	CA_CERT_PASSWORD=$(CA_CERT_PASSWORD) \
	PLAYBOOKS_HOSTS=$(PLAYBOOKS_HOSTS) \
	ELASTICSEARCH_PASSWORD=$(ELASTICSEARCH_PASSWORD)


clusterapi-wait-module-docs:
	ansible-doc -M $(ANSIBLE_COLLECTIONS_PATHS)/ansible_collections/ska_collections/clusterapi/plugins/modules \
	 -t module plugin wait_for_daemonset

clusterapi-wait-module-test:
	ANSIBLE_LIBRARY=$(ANSIBLE_COLLECTIONS_PATHS)/ansible_collections/ska_collections/clusterapi/plugins/modules \
	ansible -m wait_for_daemonset -a 'name=speaker kubectl_namespace=metallb-system initpause=0' localhost -vvv

clusterapi-patch-module-test:
	ANSIBLE_LIBRARY=$(ANSIBLE_COLLECTIONS_PATHS)/ansible_collections/ska_collections/clusterapi/plugins/modules \
	ansible -m kubectl_patch -a "name=service/ingress-nginx-controller kubectl_namespace=ingress-nginx patch='{\"spec\":{\"ports\":[{\"port\": 80, \"nodePort\": 30080},{\"port\": 443, \"nodePort\": 30443}]}}'" localhost -vv

clusterapi-setenv-module-test:
	ANSIBLE_LIBRARY=$(ANSIBLE_COLLECTIONS_PATHS)/ansible_collections/ska_collections/clusterapi/plugins/modules \
	ansible  --extra-vars='{"envvars": {"CALICO_IPV4POOL_IPIP": "Always"}}'  -m kubectl_set_env -a 'name=daemonset/calico-node kubectl_namespace=kube-system envvars="{{envvars}}"' localhost -vvv


# push: ## Push cluster. Filter with TF_TARGET
# 	@export $(TF_EXTRA_VARS) && \
# 	terraform -chdir=$(TF_ROOT_DIR) state push $(TF_ARGUMENTS) /tmp/tfstate.file

# pull:  ## Pull tf state
# 	@export $(TF_EXTRA_VARS) && \
# 	terraform -chdir=$(TF_ROOT_DIR) state pull $(TF_ARGUMENTS) > /tmp/tfstate.file
# 	cat /tmp/tfstate.file


clusterapi-vars:  ## Current variables
	@echo "Current variable settings:"
	@echo "ENVIRONMENT=$(ENVIRONMENT)"
	@echo "SERVICE=$(SERVICE)"
	@echo "GITLAB_PROJECT_ID=$(GITLAB_PROJECT_ID)"
	@echo "TF_EXTRA_VARS=$(TF_EXTRA_VARS)"

clusterapi-check-env: ## Check ENVIRONMENT variable
ifndef ENVIRONMENT
	$(error ENVIRONMENT is undefined)
endif

# # If the first argument is "install"...
# ifeq (playbooks,$(firstword $(MAKECMDGOALS)))
#   # use the rest as arguments for "playbooks"
#   TARGET_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
#   # ...and turn them into do-nothing targets
#   $(eval $(TARGET_ARGS):;@:)
# endif

# playbooks: clusterapi-check-env ## Access Ansible Collections submodule targets
# 	@cd ska-ser-ansible-collections && $(MAKE) $(TARGET_ARGS) ENVIRONMENT=$(ENVIRONMENT) SERVICE=$(SERVICE) \
# 	 PLAYBOOKS_HOSTS=$(PLAYBOOKS_HOSTS) \
# 	 GITLAB_PROJECT_ID=$(GITLAB_PROJECT_ID) \
# 	 TF_HTTP_USERNAME=$(TF_HTTP_USERNAME) \
# 	 TF_HTTP_PASSWORD=$(TF_HTTP_PASSWORD) \
# 	 TF_HTTP_ADDRESS=$(TF_HTTP_ADDRESS) \
# 	 TF_HTTP_LOCK_ADDRESS=$(TF_HTTP_LOCK_ADDRESS) \
# 	 TF_HTTP_UNLOCK_ADDRESS=$(TF_HTTP_UNLOCK_ADDRESS) \
# 	 BASE_PATH=$(THIS_BASE) \
# 	 TF_ROOT_DIR=$(TF_ROOT_DIR) \
# 	 PLAYBOOKS_ROOT_DIR=$(PLAYBOOKS_ROOT_DIR) \
# 	 ANSIBLE_CONFIG=$(ANSIBLE_CONFIG) \
# 	 ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)

# # If the first argument is "orch"...
# ifeq (orch,$(firstword $(MAKECMDGOALS)))
#   # use the rest as arguments for "orch"
#   TARGET_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
#   # ...and turn them into do-nothing targets
#   $(eval $(TARGET_ARGS):;@:)
# endif

# orch: clusterapi-check-env ## Access Orchestration submodule targets
# 	@cd ska-ser-orchestration && $(MAKE) $(TARGET_ARGS) ENVIRONMENT=$(ENVIRONMENT) SERVICE=$(SERVICE) \
# 	 PLAYBOOKS_HOSTS=$(PLAYBOOKS_HOSTS) \
# 	 GITLAB_PROJECT_ID=$(GITLAB_PROJECT_ID) \
# 	 TF_HTTP_USERNAME=$(TF_HTTP_USERNAME) \
# 	 TF_HTTP_PASSWORD=$(TF_HTTP_PASSWORD) \
# 	 TF_HTTP_ADDRESS=$(TF_HTTP_ADDRESS) \
# 	 TF_HTTP_LOCK_ADDRESS=$(TF_HTTP_LOCK_ADDRESS) \
# 	 TF_HTTP_UNLOCK_ADDRESS=$(TF_HTTP_UNLOCK_ADDRESS) \
# 	 BASE_PATH=$(THIS_BASE) \
# 	 TF_ROOT_DIR=$(TF_ROOT_DIR) \
# 	 PLAYBOOKS_ROOT_DIR=$(PLAYBOOKS_ROOT_DIR) \
# 	 ANSIBLE_CONFIG=$(ANSIBLE_CONFIG) \
# 	 ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)

# help:  ## Show Help
# 	@echo "make targets:"
# 	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
# 	@echo "";
# 	@make vars




# watch "grep -E 'PLAYBOOK|skipped' /tmp/capo-config.log "



# where and how to install yq
EXE_DIR=/usr/local/bin
SUDO_COMMAND = sudo --preserve-env=http_proxy --preserve-env=https_proxy
SUDO_FOR_EXE_DIR = $(shell if [ ! -w $(EXE_DIR) ]; then echo $(SUDO_COMMAND); fi)
OS_NAME := $(shell uname -s | tr A-Z a-z)
OS_ARCH := $(shell uname -p)
OS_BIN := amd64
ifeq ($(OS_NAME),darwin)
ifeq ($(OS_ARCH),arm)
OS_BIN := $(shell uname -m)
endif
endif


capo-yq: ## install yq command line tool
	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/yq)
	$(eval RIGHT_YQ := $(shell $(EXE_DIR)/yq --version 2>&1 | grep $(YQ_VERSION)))
	@if [ ! -f "$(EXE_DIR)/yq" ] || [ -z "$(RIGHT_YQ)" ]; then \
		echo "Installing yq version $(YQ_VERSION) from https://github.com/mikefarah/yq/"; \
		curl -Lo $(TMP_FILE) https://github.com/mikefarah/yq/releases/download/v$(YQ_VERSION)/yq_$(OS_NAME)_$(OS_BIN) && \
		$(SUDO_FOR_EXE_DIR) mv $(TMP_FILE) "$(EXE_DIR)/yq" && \
		$(SUDO_FOR_EXE_DIR) chmod +x "$(EXE_DIR)/yq"; \
	else \
		echo "yq $(YQ_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)


capo-pre-vars: capo-yq ## set vars and output tmp file so it can be read in the next target
	$(eval OPENSTACK_CLOUD_YAML_B64 := $(shell cat "$(CAPO_CLOUDS_PATH)" |  base64 --wrap=0))
	$(eval OPENSTACK_CLOUD_CACERT_B64 := "$(shell echo "" | base64 --wrap=0)")
	$(eval CAPO_AUTH_URL := $(shell echo "$(OPENSTACK_CLOUD_YAML_B64)" | base64 -d | $(EXE_DIR)/yq e .clouds.$(CAPO_CLOUD).auth.auth_url -))
	$(eval CAPO_USERNAME := $(shell echo "$(OPENSTACK_CLOUD_YAML_B64)" | base64 -d | $(EXE_DIR)/yq e .clouds.$(CAPO_CLOUD).auth.username -))
	$(eval CAPO_PASSWORD := $(shell echo "$(OPENSTACK_CLOUD_YAML_B64)" | base64 -d | $(EXE_DIR)/yq e .clouds.$(CAPO_CLOUD).auth.password -))
	$(eval CAPO_REGION := $(shell echo "$(OPENSTACK_CLOUD_YAML_B64)" | base64 -d | $(EXE_DIR)/yq e .clouds.$(CAPO_CLOUD).region_name -))
	$(eval CAPO_PROJECT_ID := $(shell echo "$(OPENSTACK_CLOUD_YAML_B64)" | base64 -d | $(EXE_DIR)/yq e .clouds.$(CAPO_CLOUD).auth.project_id -))
	$(eval CAPO_PROJECT_NAME := $(shell echo "$(OPENSTACK_CLOUD_YAML_B64)" | base64 -d | $(EXE_DIR)/yq e .clouds.$(CAPO_CLOUD).auth.project_name -))
	$(eval CAPO_DOMAIN_NAME := $(shell echo "$(OPENSTACK_CLOUD_YAML_B64)" | base64 -d | $(EXE_DIR)/yq e .clouds.$(CAPO_CLOUD).auth.user_domain_name -))
	$(eval CAPO_APPLICATION_CREDENTIAL_NAME := $(shell echo "$(OPENSTACK_CLOUD_YAML_B64)" | base64 -d | $(EXE_DIR)/yq e .clouds.$(CAPO_CLOUD).auth.application_credential_name -))
	$(eval CAPO_APPLICATION_CREDENTIAL_ID := $(shell echo "$(OPENSTACK_CLOUD_YAML_B64)" | base64 -d | $(EXE_DIR)/yq e .clouds.$(CAPO_CLOUD).auth.application_credential_id -))
	$(eval CAPO_APPLICATION_CREDENTIAL_SECRET := $(shell echo "$(OPENSTACK_CLOUD_YAML_B64)" | base64 -d | $(EXE_DIR)/yq e .clouds.$(CAPO_CLOUD).auth.application_credential_secret -))
	$(eval CAPO_DOMAIN_ID := $(shell echo "$(OPENSTACK_CLOUD_YAML_B64)" | base64 -d | $(EXE_DIR)/yq e .clouds.$(CAPO_CLOUD).auth.domain_id -))
	$(eval CAPO_CACERT_ORIGINAL := $(shell echo "$(OPENSTACK_CLOUD_YAML_B64)" | base64 -d | $(EXE_DIR)/yq e .clouds.${CAPO_CLOUD}.cacert -))

	$(eval CAPO_CLOUD_PROVIDER_CONF_TMP := $(shell mktemp /tmp/cloud.confXXX))
	@echo "[Global]" > $(CAPO_CLOUD_PROVIDER_CONF_TMP)
	@echo "auth-url=$(CAPO_AUTH_URL)" >> $(CAPO_CLOUD_PROVIDER_CONF_TMP)
	@echo "username=\"$(CAPO_USERNAME)\"" >> $(CAPO_CLOUD_PROVIDER_CONF_TMP)
	@echo "password=\"$(CAPO_PASSWORD)\"" >> $(CAPO_CLOUD_PROVIDER_CONF_TMP)
	@echo "tenant-id=\"$(CAPO_PROJECT_ID)\"" >> $(CAPO_CLOUD_PROVIDER_CONF_TMP)
	@echo "tenant-name=\"$(CAPO_PROJECT_NAME)\"" >> $(CAPO_CLOUD_PROVIDER_CONF_TMP)
	@echo "domain-name=\"$(CAPO_DOMAIN_NAME)\"" >> $(CAPO_CLOUD_PROVIDER_CONF_TMP)
	@echo "region=\"$(CAPO_REGION)\"" >> $(CAPO_CLOUD_PROVIDER_CONF_TMP)
	@if [ "$(strip $(CAPO_DOMAIN_ID))" != "null" ]; then \
		echo "domain-id=\"$(CAPO_DOMAIN_ID)\"" >> $(CAPO_CLOUD_PROVIDER_CONF_TMP); \
	fi
	@if [ "$(strip $(CAPO_APPLICATION_CREDENTIAL_NAME))" != "null" ]; then \
		echo "domain-id=\"$(CAPO_APPLICATION_CREDENTIAL_NAME)\"" >> $(CAPO_CLOUD_PROVIDER_CONF_TMP); \
	fi
	@if [ "$(strip $(CAPO_APPLICATION_CREDENTIAL_ID))" != "null" ]; then \
		echo "domain-id=\"$(CAPO_APPLICATION_CREDENTIAL_ID)\"" >> $(CAPO_CLOUD_PROVIDER_CONF_TMP); \
	fi
	@if [ "$(strip $(CAPO_APPLICATION_CREDENTIAL_SECRET))" != "null" ]; then \
		echo "domain-id=\"$(CAPO_APPLICATION_CREDENTIAL_SECRET)\"" >> $(CAPO_CLOUD_PROVIDER_CONF_TMP); \
	fi
	@if [ "$(strip $(CAPO_CACERT_ORIGINAL))" != "null" ]; then \
		echo "ca-file=\"/etc/certs/cacert\"" >> $(CAPO_CLOUD_PROVIDER_CONF_TMP); \
	fi

# read in tmp file provider config from capo-pre-vars (must force sequential targets)
capo-vars: capo-pre-vars ## generate the openstack vars
	$(eval OPENSTACK_CLOUD_PROVIDER_CONF_B64 := "$(shell cat $(CAPO_CLOUD_PROVIDER_CONF_TMP) | base64 --wrap=0)")
	@rm -f $(CAPO_CLOUD_PROVIDER_CONF_TMP)

	@echo "CAPO_CLOUDS_PATH = $(CAPO_CLOUDS_PATH)"
	@echo "OPENSTACK_CLOUD_YAML_B64 (unpacked) = \n$$( echo "$(OPENSTACK_CLOUD_YAML_B64)"  |  base64 -d)"
	@echo "OPENSTACK_CLOUD_YAML_B64 = $(OPENSTACK_CLOUD_YAML_B64)"
	@echo "OPENSTACK_CLOUD_CACERT_B64 = $(OPENSTACK_CLOUD_CACERT_B64)"
	@echo "OPENSTACK_CLOUD_PROVIDER_CONF = \n$$(echo "$(OPENSTACK_CLOUD_PROVIDER_CONF_B64)" | base64 -d)"
	@echo "OPENSTACK_CLOUD_PROVIDER_CONF_B64 = $(OPENSTACK_CLOUD_PROVIDER_CONF_B64)"
	@echo "OPENSTACK_CLOUD = $(OPENSTACK_CLOUD)"
	@echo "CAPO_AUTH_URL = $(CAPO_AUTH_URL)"
	@echo "CAPO_USERNAME = $(CAPO_USERNAME)"
	@echo "CAPO_PASSWORD = $(CAPO_PASSWORD)"
	@echo "CAPO_REGION = $(CAPO_REGION)"
	@echo "CAPO_PROJECT_ID = $(CAPO_PROJECT_ID)"
	@echo "CAPO_PROJECT_NAME = $(CAPO_PROJECT_NAME)"
	@echo "CAPO_DOMAIN_NAME = $(CAPO_DOMAIN_NAME)"
	@echo "CAPO_DOMAIN_ID = $(CAPO_DOMAIN_ID)"
	@echo "CAPO_APPLICATION_CREDENTIAL_NAME = $(CAPO_APPLICATION_CREDENTIAL_NAME)"
	@echo "CAPO_APPLICATION_CREDENTIAL_ID = $(CAPO_APPLICATION_CREDENTIAL_ID)"
	@echo "CAPO_APPLICATION_CREDENTIAL_SECRET = $(CAPO_APPLICATION_CREDENTIAL_SECRET)"

capo-controlplane:  ## Get controlplane
	$(eval FIRST_MASTER := $(shell kubectl get openstackmachines -l "cluster.x-k8s.io/cluster-name=$(CAPO_CLUSTER),cluster.x-k8s.io/control-plane=" -o json | jq -r .items[0].spec.providerID | cut -d '/' -f 3))
	@echo "First master: $(FIRST_MASTER)"

capo-get-kubeconfig:  ## get kubeconfig
	unset KUBECONFIG; kubectl get secret/$(CAPO_CLUSTER)-kubeconfig -o json | jq -r .data.value   | base64 --decode > $(CAPO_CLUSTER)-kubeconfig

capo-k9s: capo-get-kubeconfig
	export KUBECONFIG=$(THIS_BASE)/$(CAPO_CLUSTER)-kubeconfig; \
	k9s

capo-clean:  ## remove the cluster template source
	rm -f kustomize/base/cluster-template.orig.yaml

kustomize/base/cluster-template.orig.yaml:  ## get the cluster template source
	@# curl -L https://github.com/kubernetes-sigs/cluster-api-provider-openstack/releases/download/$(CAPO_VERSION)/cluster-template-without-lb.yaml -o kustomize/base/cluster-template.orig.yaml
	@echo "getting the latest version of CAPO..."
	VERS=$$(curl -s -H "Accept: application/json" -L `clusterctl config repositories | grep openstack | awk '{print $$3}'` | jq -r .tag_name); \
	echo "got VERS=$${VERS}"; \
	curl -L https://github.com/kubernetes-sigs/cluster-api-provider-openstack/releases/download/$${VERS}/cluster-template.yaml -o kustomize/base/cluster-template.orig.yaml
	ls -latr kustomize/base/cluster-template.orig.yaml

	@# curl -L https://raw.githubusercontent.com/kubernetes-sigs/cluster-api-provider-openstack/main/templates/cluster-template.yaml -o kustomize/base/cluster-template.orig.yaml

refresh: capo-clean kustomize/base/cluster-template.orig.yaml ## refresh the cluster template source

rook-etc: ## check for the /etc/ceph directory
	@if [ "$(shell ls -d $(ETC_CEPH))" != "$(ETC_CEPH)" ]; then \
	echo "rook: $(ETC_CEPH) is missing"; \
	exit 1; \
	fi

rook-vars: rook-etc ## Generate all the rook.io vars
	$(eval ROOK_EXTERNAL_FSID := $(shell grep fsid $(ETC_CEPH_CONF) | awk -F ' *= *' '{ print $$2 }'))
	$(eval ROOK_EXTERNAL_ADMIN_SECRET := $(shell sudo grep "key =" $(ETC_CEPH_CLIENT_ADMIN_KEY_RING) | awk '{print $$3}'))
	$(eval ROOK_EXTERNAL_ADMIN_KEY := $(shell sudo grep "key " $(ETC_CEPH_CLIENT_ADMIN_KEY_RING) | python -c "import sys; print(sys.stdin.readline().rstrip().split()[2]);"))
	$(eval ROOK_EXTERNAL_CEPH_MON_DATA := $(shell grep -E "mon.host " $(ETC_CEPH_CONF) | sed 's/mon host /mon_host /' | python -c "import sys; x=['a','b','c','d','e']; print(\",\".join([x.pop(0)+'='+l.split(':',1)[1].strip('[]').replace('/0','') for l in sys.stdin.readline().replace('[', '').replace(']', '').rstrip().split()[2].strip('[]').split(',') if l.split(':',1)[0] == 'v2']));"))
	@# swap '"' for QTE as eval does not preserve them
	$(eval ROOK_EXTERNAL_CEPH_MONITORS := $(shell grep -E "mon.host " $(ETC_CEPH_CONF) | sed 's/mon host /mon_host /' | python -c "import sys; print(\",\".join(['QTE'+l.split(':',1)[1].strip('[]').replace('/0','')+'QTE' for l in sys.stdin.readline().replace('[', '').replace(']', '').rstrip().split()[2].strip('[]').split(',') if l.split(':',1)[0] == 'v1']));"))
	$(eval CSI_RBD_NODE_SECRET := $(ROOK_EXTERNAL_ADMIN_SECRET))
	$(eval CSI_RBD_PROVISIONER_SECRET := $(ROOK_EXTERNAL_ADMIN_SECRET))
	$(eval CSI_CEPHFS_NODE_SECRET := $(ROOK_EXTERNAL_ADMIN_SECRET))
	$(eval CSI_CEPHFS_PROVISIONER_SECRET := $(ROOK_EXTERNAL_ADMIN_SECRET))
	@echo "ETC_CEPH_CONF=$(ETC_CEPH_CONF)"
	@echo "ETC_CEPH_CLIENT_ADMIN_KEY_RING=$(ETC_CEPH_CLIENT_ADMIN_KEY_RING)"
	@echo "ROOK_EXTERNAL_FSID=$(ROOK_EXTERNAL_FSID)"
	@echo "ROOK_EXTERNAL_ADMIN_KEY=$(ROOK_EXTERNAL_ADMIN_KEY)"
	@echo "ROOK_EXTERNAL_ADMIN_SECRET=$(ROOK_EXTERNAL_ADMIN_SECRET)"
	@echo "ROOK_EXTERNAL_CEPH_MON_DATA=$(ROOK_EXTERNAL_CEPH_MON_DATA)"
	@echo "ROOK_EXTERNAL_CEPH_MONITORS=$(ROOK_EXTERNAL_CEPH_MONITORS)"
	@echo "CSI_RBD_NODE_SECRET=$(CSI_RBD_NODE_SECRET)"
	@echo "CSI_RBD_PROVISIONER_SECRET=$(CSI_RBD_PROVISIONER_SECRET)"
	@echo "CSI_CEPHFS_NODE_SECRET=$(CSI_CEPHFS_NODE_SECRET)"
	@echo "CSI_CEPHFS_PROVISIONER_SECRET=$(CSI_CEPHFS_PROVISIONER_SECRET)"

capo-cluster-template: kustomize/base/cluster-template.orig.yaml capo-vars ## generate the cluster manifest template
	kubectl kustomize ./kustomize/overlay/$(KUSTOMIZE_OVERLAY)/ > $(THIS_BASE)/$(CAPO_CLUSTER)-template.yaml

capo-cluster-manifest: rook-vars capo-cluster-template  ## configure and create capo cluster manifest
	@# wget https://github.com/vmware-tanzu/cluster-api-provider-bringyourownhost/releases/download/v0.2.0/cluster-template.yaml
	@# --infrastructure byoh

	export \
	KUBE_NAMESPACE="$(KUBE_NAMESPACE)" \
	ROOK_NAMESPACE="$(CEPH_NAMESPACE)" \
	OPENSTACK_FAILURE_DOMAIN="$(OPENSTACK_FAILURE_DOMAIN)" \
	OPENSTACK_IMAGE_NAME="$(OPENSTACK_IMAGE_NAME)" \
	OPENSTACK_CLOUD="$(OPENSTACK_CLOUD)" \
	OPENSTACK_CONTROL_PLANE_MACHINE_FLAVOR="$(OPENSTACK_CONTROL_PLANE_MACHINE_FLAVOR)" \
	OPENSTACK_DNS_NAMESERVERS="$(OPENSTACK_DNS_NAMESERVERS)" \
	OPENSTACK_NODE_MACHINE_FLAVOR="$(OPENSTACK_NODE_MACHINE_FLAVOR)" \
	OPENSTACK_SSH_KEY_NAME="$(OPENSTACK_SSH_KEY_NAME)" \
	CAPO_CLOUD="$(CAPO_CLOUD)" \
	OPENSTACK_EXTERNAL_NETWORK_ID="$(OPENSTACK_EXTERNAL_NETWORK_ID)" \
	OPENSTACK_CLOUD_YAML_B64="$(OPENSTACK_CLOUD_YAML_B64)" \
	OPENSTACK_CLOUD_CACERT_B64="$(OPENSTACK_CLOUD_CACERT_B64)" \
	OPENSTACK_CLOUD_PROVIDER_CONF_B64="$(OPENSTACK_CLOUD_PROVIDER_CONF_B64)" \
	CLUSTER_API_OPENSTACK_INSTANCE_CREATE_TIMEOUT="$(CLUSTER_API_OPENSTACK_INSTANCE_CREATE_TIMEOUT)" \
	NAMESPACE="$(CEPH_NAMESPACE)" \
	ROOK_EXTERNAL_FSID="$(ROOK_EXTERNAL_FSID)" \
	ROOK_EXTERNAL_ADMIN_SECRET="$(ROOK_EXTERNAL_ADMIN_SECRET)" \
	ROOK_EXTERNAL_ADMIN_KEY="$(ROOK_EXTERNAL_ADMIN_KEY)" \
	ROOK_EXTERNAL_CEPH_MON_DATA="$(ROOK_EXTERNAL_CEPH_MON_DATA)" \
	ROOK_EXTERNAL_CEPH_MONITORS="$(ROOK_EXTERNAL_CEPH_MONITORS)" \
	RBD_POOL_NAME="$(RBD_POOL_NAME)" \
	RGW_POOL_PREFIX="$(RGW_POOL_PREFIX)" \
	CSI_RBD_NODE_SECRET_NAME="$(CSI_RBD_NODE_SECRET_NAME)" \
	CSI_RBD_PROVISIONER_SECRET_NAME="$(CSI_RBD_PROVISIONER_SECRET_NAME)" \
	CSI_CEPHFS_NODE_SECRET_NAME="$(CSI_CEPHFS_NODE_SECRET_NAME)" \
	CSI_CEPHFS_PROVISIONER_SECRET_NAME="$(CSI_CEPHFS_PROVISIONER_SECRET_NAME)" \
	CSI_RBD_NODE_SECRET="$(CSI_RBD_NODE_SECRET)" \
	CSI_RBD_PROVISIONER_SECRET="$(CSI_RBD_PROVISIONER_SECRET)" \
	CSI_CEPHFS_NODE_SECRET="$(CSI_CEPHFS_NODE_SECRET)" \
	CSI_CEPHFS_PROVISIONER_SECRET="$(CSI_CEPHFS_PROVISIONER_SECRET)" \
	BUNDLE_LOOKUP_TAG="$(CAPO_K8S_VERSION)" \
	K8S_VERSION="$(CAPO_K8S_VERSION)" \
	CALICO_VERSION="$(CALICO_VERSION)" \
	NGINX_VERSION="$(NGINX_VERSION)" \
	METALLB_VERSION="$(METALLB_VERSION)" \
	ROOK_VERSION="$(ROOK_VERSION)" \
	NETWORK_NAME="$(NETWORK_NAME)" \
	SUBNET_NAME="$(SUBNET_NAME)" \
	CIDR_BLOCK="$(CIDR_BLOCK)" \
	IP_AUTODETECTION_METHOD="$(IP_AUTODETECTION_METHOD)" \
	CALICO_IPV4POOL_IPIP="$(CALICO_IPV4POOL_IPIP)" \
	HELM_VERSION=$(HELM_VERSION) \
	CONTROL_PLANE_COUNT=$(CONTROL_PLANE_COUNT) \
	WORKER_COUNT=$(WORKER_COUNT) ; \
	printenv | grep -E 'ROOK|CSI|RBD|NAMESPACE'; \
	clusterctl generate cluster $(CAPO_CLUSTER) \
		--target-namespace $(KUBE_NAMESPACE) \
		--kubernetes-version $${K8S_VERSION} \
		--control-plane-machine-count $(CONTROL_PLANE_COUNT) \
		--worker-machine-count $(WORKER_COUNT) --from $(THIS_BASE)/$(CAPO_CLUSTER)-template.yaml > $(CAPO_CLUSTER).yaml; \

capo-cluster: capo-cluster-manifest ## Generate and apply the cluster manifest
	kubectl apply -f $(CAPO_CLUSTER).yaml

capo-delete:
	kubectl delete -f $(CAPO_CLUSTER).yaml || true

capo-cp:  ## get control plane IPs
	openstack server list -f value -c Name -c Status -c Networks | \
	grep $(CAPO_CLUSTER)-control-plane | \
	grep ACTIVE | \
	perl -ne "s/^.*?((192|10).+?)'.*?$$/\$$1/; print"

capo-cp-ssh: capo-get-kubeconfig  ## ssh to first controlplane node
	export KUBECONFIG=$(THIS_BASE)/$(CAPO_CLUSTER)-kubeconfig; \
	export IP=`kubectl get $$(kubectl get openstackmachine -o name | head -1) -o json | jq -r -e '.status.addresses[0].address'`; \
	ssh ubuntu@$${IP}

clusterctl:  ## install clusterctl
	curl -L https://github.com/kubernetes-sigs/cluster-api/releases/download/$(CLUSTERCTL_VERSION)/clusterctl-linux-amd64 -o clusterctl
	chmod +x ./clusterctl
	sudo mv ./clusterctl /usr/local/bin/clusterctl
	clusterctl version

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
production: false
